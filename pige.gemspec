# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = "pige"
  spec.version       = "0.0.1"
  spec.authors       = ["Clemence Lelong"]
  spec.email         = ["lelongclemence@gmail.com"]
  spec.summary       = %q{Pige de noel}
  spec.description   = %q{Pige de noel : technical test for ubisoft}
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "rspec-redis_helper"
  spec.add_development_dependency "simplecov"

  spec.add_dependency "thor", '~> 0' #command line interface
  spec.add_dependency "redis", '>= 3.2.0' #cache
  spec.add_dependency "redis_orm", '>= 0.7' #data structure
end
