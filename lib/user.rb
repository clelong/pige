class User < RedisOrm::Base
  property :first_name, String, unique: true
  property :last_name, String

  index :first_name
  index [:first_name, :last_name]

  has_one :user, :as => :partner

  has_many :pige

  # def define_partner user
  #   self.partner = user
  #   user.partner = self
  # end
end
