class Cli < Thor

  $redis = Redis.new(:host => 'localhost', :port => 6379)

  desc 'create <PIGE NAME>', 'create a pige'
  def create pige_name
    p=Pige.find_by_name name: pige_name
    if p.nil?
      Pige.create name: pige_name
      puts "You have created the pige #{pige_name}."
    else
      puts "#{pige_name} already exists."
    end
  end

  desc "register <FIRST NAME> <LAST NAME>", 'register user to a pige'
  options pige: :required, aliases: :p
  def register firstname, lastname
    u=User.find_by_first_name_and_last_name(firstname,lastname)
    if u.nil?
      u=User.new first_name: firstname, last_name: lastname
      u.save
    end

    pige_name=options[:pige]
    p=Pige.find_by_name pige_name
    unless !p.nil?
      p=Pige.new name: pige_name
      p.save
    end
    p.register u
    puts "Hello #{firstname} #{lastname}, you are registered for the pige called '#{options[:pige]}'."
  end

  desc "add_partner <FIRST NAME> <LAST NAME>", 'define partner to someone'
  options to: :required, banner: '<FIRST NAME> <LAST NAME>'
  def add_partner firstname, lastname
    p=User.find_by_first_name_and_last_name(firstname,lastname)
    if p.nil?
      p=User.new first_name: firstname, last_name: lastname
      p.save
    end

    to_first_name=options[:to].split(' ').first
    to_last_name=options[:to].split(' ').last

    u=User.find_by_first_name_and_last_name(to_first_name, to_last_name)
    if u.nil?
      u=User.new first_name: to_first_name, last_name: to_last_name
      u.save
    end
    u.partner = p
    p.partner = u
    p.save
    u.save
    puts "Hello #{to_first_name} #{to_last_name}, #{firstname} #{lastname} is now your partner."
  end

  desc 'start <NAME>', 'run a pige'
  def start name
    p=Pige.find_by_name name
    puts p.run_pige
    puts p.show_all
  end

  desc 'show_members <NAME>', 'show pige members'
  def show_members name
    p=Pige.find_by_name name
    p.members.each do |m|
      puts "#{m.first_name} #{m.last_name}"
    end
  end

  desc 'show <NAME>', 'show gift receivers'
  option :user
  def show name
    p=Pige.find_by_name name
    u=options[:user]
    if !u.nil?
      to_first_name=u.split(' ').first
      to_last_name=u.split(' ').last
      u=User.find_by_first_name_and_last_name(to_first_name, to_last_name)
      if !u.nil?
        puts p.show u
      else
        puts 'Sorry, this user has not been found!'
      end
    else
     puts p.show_all
    end
  end
end
