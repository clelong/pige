require 'thor'
require 'redis'
require 'redis_orm'
require 'user'
require 'cli'

class Pige < RedisOrm::Base
  property :name, String
  #property :sorted_set1, Array #doesnt work!!!
  #property :sorted_set2, Array #doesnt work!!!

  index :name

  has_one :users, as: :responsable
  has_many :users, as: :members

  def register user
    self.members << user
  end

  def show_all
    rlist1=$redis.lrange("pige:#{self.id}:list1",0,-1)
    rlist2=$redis.lrange("pige:#{self.id}:list2",0,-1)

    unless rlist1.nil?
      output=''
      rlist1.each_with_index do |k,v|
        u1=User.find(k.to_i)
        u2=User.find(rlist2[v.to_i])
        output = output + "#{u1.first_name} #{u1.last_name} <--> #{u2.first_name} #{u2.last_name}\n"
      end
    else
      output = "#{self.name} has not started yet. Please type the following commande: pige start #{self.name}"
    end
    return output
  end

  def show user
    associate = self.find user
    if !associate.nil?
      return "#{associate.first_name} #{associate.last_name}"
    else
      return "Sorry, this user is not a member of #{self.name}!"
    end
  end

  def run_pige
    unless !is_big_enough
      valid=false
      while !valid
        list1 = self.members.shuffle
        list2 = self.members.shuffle
        if Pige::is_pige_valid(list1,list2)
          valid=true
        end
      end
      self.set_redis_list(list1.map(&:id),list2.map(&:id)) #save in redis!
      return "#{self.name} has been started!"
    else
      return "#{self.name} has not enough members!"
    end
  end

  def find user
    rlist1=$redis.lrange("pige:#{self.id}:list1",0,-1)
    rlist2=$redis.lrange("pige:#{self.id}:list2",0,-1)

    unless rlist1.empty?
      index = rlist1.index(user.id.to_s)
      if !index.nil?
        return User.find(rlist2[index].to_i)
      else
        return nil
      end
    end
  end

  def is_big_enough
    if self.members.size >=2
      if self.members.size == 2
        if self.members.first.first_name == self.members.last.partner.first_name
          return false
        end
      end
    else
      return false
    end
    return true
  end

  def set_redis_list(rlist1,rlist2)
    $redis.del("pige:#{self.id}:list1")
    $redis.del("pige:#{self.id}:list2")
    $redis.pipelined do
      rlist1.each{ |v| $redis.rpush("pige:#{self.id}:list1",v) }
      rlist2.each{ |v| $redis.rpush("pige:#{self.id}:list2",v) }
    end
  end

  private



  def self.is_pige_valid(list1,list2)
    index=0
    while index<=(list1.size-1)
      if !is_association_valid(list1[index],list2[index])
        return false
      end
      index=index+1
    end
    return true
  end

  def self.is_association_valid(user1,user2)
    user1_id=user1.id
    user2_id=user2.id
    unless user1_id == user2_id || \
      user1_id == (user2.partner.nil? ? nil : user2.partner.id) \
      || user2_id == (user1.partner.nil? ? nil : user1.partner.id)
      return true
    else
      return false
    end
  end
end
