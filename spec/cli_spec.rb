require 'pige'

describe Cli do
  before(:each) do
    %x( pige create pigetest )
    %x( pige register julien pellet --pige pigetest )
    %x( pige register clemence pellet --pige pigetest )
    %x( pige register lili pellet --pige pigetest )
    %x( pige register pacome pellet --pige pigetest )
    %x( pige register jp pellet --pige pigetest )
    %x( pige register babette pellet --pige pigetest )
    %x( pige add_partner babette pellet --to 'jp pellet' )
    %x( pige add_partner clemence pellet --to 'julien pellet' )
    %x( pige start pigetest )
  end

  describe "#create" do
    it "should return pige name" do
      expect(%x( pige create pigetest2 )).to eq("You have created the pige pigetest2.\n")
    end
  end

  describe "#register" do
    it "should return registered member" do
      expect(%x( pige register melanie pellet --pige pigetest2 )).to eq("Hello melanie pellet, you are registered for the pige called 'pigetest2'.\n")
    end
  end

  describe "#add_partner" do
    it "should return partner name" do
      expect(%x( pige add_partner melanie pellet --to 'bertrand pellet' )).to eq("Hello bertrand pellet, melanie pellet is now your partner.\n")
    end
  end

  describe "#start" do
    it "should return a message to say that the pige is started" do
      expect(%x( pige start pigetest )).to start_with("pigetest has been started!")
    end
  end

  describe "#show_members" do
    it "should returns all members" do
      expect(%x( pige show_members pigetest)).not_to eq("")
    end
  end

  describe "#show" do
    it "should return a random user if no option" do
      expect(%x( pige show pigetest --user 'pacome pellet' )).to end_with("pellet\n")
    end
    it "should return all pige associations if option --all" do
      expect(%x( pige show pigetest )).not_to eq("")
    end
  end
end