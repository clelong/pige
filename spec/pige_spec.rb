require 'redis'
require 'redis_orm'
require 'pige'

describe Pige do
  before(:each) do
    $redis = Redis.new(:host => 'localhost', :port => 6379)
    $redis.flushdb
    @clemence = User.create first_name: 'clemence', last_name: 'pellet'
    @julien = User.create first_name: 'julien', last_name: 'pellet'
    @clemence.partner=@julien
    @clemence.save
    @julien.partner=@clemence
    @julien.save
    @pacome = User.create first_name: 'pacome', last_name: 'pellet'
    @lili = User.create first_name: 'lili', last_name: 'pellet'
    @jp = User.create first_name: 'jp', last_name: 'pellet'
    @babette = User.create first_name: 'babette', last_name: 'pellet'
    @jp.partner=@babette
    @jp.save
    @babette.partner=@jp
    @babette.save
    @pige = Pige.create name: 'mypige'
    @pige.register @clemence
    @pige.register @julien
    @pige.register @pacome
    @pige.register @lili
    @pige.register @jp
    @pige.register @babette
  end

  describe "#register" do
    it "should return 1 if the person is registered" do
      expect(@pige.members.map(&:first_name).include? 'clemence').to eq(true)
    end
  end

  describe "#is_big_enough" do
    before(:each) do
      @pige2 = Pige.create name: 'mypige2'
      @pige2.register @clemence
    end
    it "should return false if pige size == 1" do
      expect(@pige2.is_big_enough).to eq(false)
    end
    it "should return false if pige has 2 members that are partners" do
      @pige2.register @julien
      @clemence.partner=@julien
      @clemence.save
      @julien.partner=@clemence
      @julien.save
      expect(@pige2.is_big_enough).to eq(false)
    end

  end

  describe "#run_pige" do
    it "should be empty if not started" do
      rlist1=$redis.lrange("pige:#{@pige.id}:list1",0,-1)
      expect(rlist1.empty?).to eq(true)
    end
    it "should not be empty if started" do
      @pige.run_pige
      rlist1=$redis.lrange("pige:#{@pige.id}:list1",0,-1)
      expect(rlist1.empty?).to eq(false)
    end
    it "should return message if not enough members" do
      @pige3 = Pige.create name: 'mypige3'
      @pige3.register @clemence
      expect(@pige3.run_pige).to eq("#{@pige3.name} has not enough members!")
    end

    describe "#is_pige_valid" do
      it "should return true if pige valid" do
        list1 = [@clemence,@julien,@lili,@pacome]
        list2 = [@lili,@pacome,@clemence,@julien]
        expect(Pige.is_pige_valid(list1,list2)).to eq(true)
      end
      it "should return false if pige not valid" do
        list1 = [@clemence,@julien,@lili,@pacome]
        list2 = [@clemence,@lili,@pacome,@julien]
        expect(Pige.is_pige_valid(list1,list2)).to eq(false)
      end
    end

    describe "#is_association_valid" do
      it "should return false if users are partners" do
        expect(Pige.is_association_valid(@clemence,@julien)).to eq(false)
      end
      it "should return false if it is both the same user" do
        expect(Pige.is_association_valid(@clemence,@clemence)).to eq(false)
      end
    end

  end

  describe "#show_all" do
    it "should not be empty if pige started" do
      @pige.run_pige
      expect(@pige.show_all).not_to be_empty
    end

    it "should be empty if pige not started" do
      expect(@pige.show_all).not_to eq("#{@pige.name} has not started yet. Please type the following commande: pige start #{@pige.name}")
    end
  end

  describe "#find" do
    it "should return a random user" do
      @pige.run_pige
      expect((@pige.find @clemence).to_s).not_to eq('')
    end
  end

  describe "#show" do
    before(:each) do
      @pige.run_pige
    end
    it "should return a random user" do
      expect(@pige.show @clemence).not_to eq('')
    end

    it "should not return the same user as itself" do
      expect((@pige.show @clemence).to_s).not_to eq('clemence pellet')
    end

    it "should not return his partner" do
      expect(@pige.show @clemence).not_to eq('julien pellet')
    end

    it "should return message if the user is not a member of the pige" do
      @marie = User.create first_name: 'marie', last_name: 'lelong'
      expect(@pige.show @marie).to eq("Sorry, this user is not a member of #{@pige.name}!")
    end

  end
end