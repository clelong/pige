require 'redis'
require 'redis_orm'
require 'user'

describe User do
  before(:each) do
    $redis = Redis.new(:host => 'localhost', :port => 6379)
    $redis.flushdb
    @clemence = User.create first_name: 'clemence', last_name: 'pellet'
    @julien = User.create first_name: 'julien', last_name: 'pellet'
  end

  it "should return a user firstname" do
    expect(@clemence.first_name).to eq("clemence")
  end

  it "should return a user lastname" do
    expect(@clemence.last_name).to eq("pellet")
  end

  it "should count 2 users" do
    expect(User.count).to eq(2)
  end

  it "should find a user by his firstname" do
    expect(User.find_by_first_name('clemence').first_name).to eq('clemence')
  end

  it "should find a user by his first_name and lastname" do
    expect(User.find_by_first_name_and_last_name('clemence','pellet').first_name).to eq('clemence')
  end
end