# Pige

Don't waste paper anymore, use Pige! Pige is a great command line tool for christmas. Create a pige, add some members, and run it! the system will associates each member to another person in order to offer him a present.

## Installation

I suppose you have ruby and rubygems installed on your machine. You need also Redis so please follow these instruction if you don't have Redis installed on your machine http://redis.io/topics/quickstart .

On ubuntu :

```
apt-get install redis-server
```
Then

```
gem install pige-0.0.1.gem
```

## Usage

Here are the commands available:

```
Commands:
  pige add_partner <FIRST NAME> <LAST NAME> --to=TO   # define partner to someone
  pige create <PIGE NAME>                             # create a pige
  pige help [COMMAND]                                 # Describe available commands or one specific command
  pige register <FIRST NAME> <LAST NAME> --pige=PIGE  # register user to a pige
  pige show_members <NAME>                            # show pige members
  pige start <NAME>                                   # run a pige
```

Example:

```
pige create mypige
pige register clemence pellet --pige mypige
pige register julien pellet --pige mypige
pige register lili pellet --pige mypige
pige register pacome pellet --pige mypige
pige register jp pellet --pige mypige
pige register babette pellet --pige mypige
pige add_partner clemence pellet --to 'julien pellet'
pige add_partner babette pellet --to 'jp pellet'
pige start mypige
pige show mypige
pige show mypige --user 'clemence pellet'
```

please use redis-cli to connect to Redis and the command flushdb to clean after using the Gem.

##TODO

* Replace RedisOrm by ActiveRecords. I choose to use this solution because there was no need for a database for now and you can switch easily to ActiveRecord but it is not a scalable solution. Redis is not made to manage relationships. So in the future, I would only keep Redis to store the 2 lists because Redis is really performant to retrieve lists and manage them.
* Improve test coverage
* Install Rack server to start a Web interface
* Improve test for thor client command line, it would be could to mock Redis
* Refactor (sometimes I voluntarily let some code instead of creating a new method because with ActiveRecords there is already a method for that like find_create methods) and check other possible cases

## Contributing

1. Fork it ( https://github.com/[my-github-username]/pige/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
